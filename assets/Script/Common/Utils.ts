import { DataMgr } from "../Manager/DataMgr";

enum KeyState {
  RIGHT = "Right",
  LEFT = "Left",
  UP = "Up",
  DOWN = "Down",
}

class DataItem {
  private dataObj: object = {};

  public getData(name: string): any {
    return this.dataObj[name];
  }

  public addData(name: string, data: any): void {
    this.dataObj[name] = data;
  }
}

function createDtMgr(name: string, obj: DataMgr): void {
  obj.addManager(name, new DataItem());
}

export { createDtMgr, DataItem, KeyState };
