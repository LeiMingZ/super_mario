let Utils={};

Utils.createDtMgr=(name,obj)=>{
    let myDtMgr={
        dataObj:{},
        addData(name,data){
            this.dataObj[name]=data;
        },
        getData(name){
            return this.dataObj[name];
        }
    };
    obj.addManager(name,myDtMgr);
}

Utils.deepClone=(obj)=>{
    let result=Array.isArray(obj)?[]:{};
    for(let key in obj){
        if(typeof obj[key]==='object'){
            result[key]=Utils.deepClone(obj[key]);
        }
        else{
            result[key]=obj[key];
        }
    }
    return result;
}

module.exports =Utils;