import getDataMgr from "../Manager/DataMgr";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TileMapCtrl extends cc.Component {
  start() {
    this.node.getComponent(cc.TiledMap).tmxAsset = getDataMgr()
      .getManager("MapDtMgr")
      .getData("MarioMap1");
  }
}
