import { KeyState } from "../Common/Utils";
import getDataMgr from "../Manager/DataMgr";
import { MoveCtrl } from "./Ctrl/MoveCtrl";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Player extends cc.Component {
  @property({
    displayName: "加速度",
  })
  public accel: cc.Vec2 = cc.v2(0, 0);
  @property({
    displayName: "减速度",
  })
  public deceleration: cc.Vec2 = cc.v2(0, 0);
  @property({
    displayName: "最大速度",
  })
  public maxSpeed: cc.Vec2 = cc.v2(300, 300);

  public speed: cc.Vec2 = cc.v2(0, 0);
  public keyState: Map<KeyState, boolean> = new Map();
  public keyObj: object = null;
  public moveCtrl: MoveCtrl = null;
  public init(): void {
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onkeyDown, this);
    cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onkeyUp, this);
    this.keyObj = getDataMgr().getManager("JsonDtMgr").getData("keyCode");
    //移动控制脚本
    this.moveCtrl = new MoveCtrl(this);
    //状态控制脚本
    // this.stateCtrl = new StateCtrl(this);
    // //当前状态
    // this.state = "none";
  }
  public onkeyDown(event: any): void {
    const key = this.keyObj[event.keyCode];
    if (key === undefined) return;
    this.keyState[this.keyObj[event.keyCode]] = true;
    //this.stateCtrl.changeState(this.keyObj[event.keyCode]);
  }
  public onkeyUp(event: any): void {
    const key = this.keyObj[event.keyCode];
    if (key === undefined) return;
    this.keyState[this.keyObj[event.keyCode]] = false;
  }
  public update(dt: number): void {
    this.moveCtrl.upData(dt);
  }
}
