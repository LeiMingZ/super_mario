class MoveCtrl {
    constructor(target) {
        this.owner = target;
    }
    upData(dt) {
        let dirX = (this.owner.stateObj.Right ? 1 : 0) + (this.owner.stateObj.Left ? -1 : 0);
        if (!dirX && this.speed.x) {
            dirX = this.speed.x > 0 ? -0.5 : 0.5;
            this.speed.x += (dirX) * dt * this.accel.x;
            if (dirX * this.speed.x >= 0) {
                this.speed.x = 0;
                this.owner.stateCtrl.changeState('idel');
            }
        }
        else {
            this.speed.x += (dirX) * dt * this.accel.x;
            if(Math.abs(this.speed.x)>this.maxSpeed.x){
                this.speed.x=this.maxSpeed.x*dirX;
            }
        }
        this.owner.node.x += this.speed.x * dt;
    }
}

module.exports = MoveCtrl;