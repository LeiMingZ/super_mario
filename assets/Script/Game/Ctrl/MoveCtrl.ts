import { KeyState } from "../../Common/Utils";
import Player from "../Player";
class MoveCtrl {
  public owner: Player = null;
  constructor(target: Player) {
    this.owner = target;
  }
  public upData(dt: number): void {
    const { accel, maxSpeed, deceleration, keyState, speed } = this.owner;

    /**
     * 计算X方向
     */
    let dirX =
      (keyState[KeyState.RIGHT] ? 1 : 0) + (keyState[KeyState.LEFT] ? -1 : 0);
    /**
     * 计算加速还是减速
     */
    if ((dirX > 0 && speed.x >= 0) || (dirX < 0 && speed.x <= 0)) {
      /**
       * 加速
       */
      speed.x += dirX * dt * accel.x;
      if (Math.abs(speed.x) > maxSpeed.x) {
        speed.x = maxSpeed.x * dirX;
      }
      this.owner.node.x += speed.x * dt;
      this.owner.speed = speed;
    } else if (!dirX && !speed.x) {
      /**
       * 静止
       */
    } else {
      /**
       * 减速
       */
      dirX = dirX ? dirX : speed.x > 0 ? -1 : 1;
      speed.x += dirX * dt * deceleration.x;
      this.owner.node.x += speed.x * dt;
      this.owner.speed.x = speed.x < 0 === this.owner.speed.x < 0 ? speed.x : 0;
    }
  }
}

export { MoveCtrl };
