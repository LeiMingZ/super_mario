class StateCtrl {
    constructor(target) {
        this.owner = target;
        this.anim=this.owner.getComponent(cc.Animation);
    }
    changeState(state) {
        if (state === this.owner.state) {
            return;
        }
        this.owner.state=state;
        if('idel'===state){
            let curAniClip=this.anim.currentClip;
            if(!curAniClip){
                return;
            }
            this.anim.play(curAniClip.name,0);
            this.anim.sample(curAniClip.name);
            this.anim.stop();
            return;
            return;
        }
        let name=this._getAnimationName(state);
        this.anim.play(name);
    }
    _getAnimationName(state) {
        let obj = {
            'Left': 'BigWalk',
            'Right': 'BigWalk'
        }
        this.owner.node.scaleX = state === 'Left' ? -1 : 1;
        return obj[state];
    }
}

module.exports = StateCtrl;