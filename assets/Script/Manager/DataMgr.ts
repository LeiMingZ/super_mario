import { DataItem } from "../Common/Utils";
const { ccclass } = cc._decorator;

@ccclass("DataMgr")
class DataMgr {
  private objMgr: object = {};

  public addManager(name: string, obj: DataItem): void {
    if (!this.objMgr[name]) {
      this.objMgr[name] = obj;
    }
  }

  public getManager(name: string): DataItem {
    return this.objMgr[name];
  }

  public removeManager(name: string): void {
    delete this.objMgr[name];
  }

  public clear(): void {
    for (let key in this.objMgr) {
      delete this.objMgr[key];
    }
  }
}

let instance = null;

export default function getDataMgr(): DataMgr {
  if (!instance) {
    instance = new DataMgr();
  }
  return instance;
}

export { DataMgr };
