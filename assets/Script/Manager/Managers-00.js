//创建这个的目的是为了要存储所有被总控制脚本管理的所有脚本对象。
class Managers{
    constructor(){
        console.log('Managers constructor');
        this.objMgr ={};
    }

    addManager(name,obj){
        if(!this.objMgr[name]){
            this.objMgr[name] = obj;
        }
    }

    getManager(name){
        return this.objMgr[name];
    }

    removeManager(name){
        delete this.objMgr[name];
    }

    clear(){
        for(let key in this.objMgr){
            delete this.objMgr[key];
        }
    }

    //可以一次性初始化这个管理者所存储的所有被game管理的脚本对象的init
    initManager(){
        for(let key in this.objMgr){
            if(this.objMgr[key].init){
                this.objMgr[key].init();
            }
        }
    }

    lateInitManager(){
        for(let key in this.objMgr){
            if(this.objMgr[key].lateInit){
                this.objMgr[key].lateInit();
            }
        }
    }

    upDateManager(dt){
        for(let key in this.objMgr){
            if(this.objMgr[key].upDate){
                this.objMgr[key].upDate(dt);
            }
        }
    }

    lateUpDateManager(dt){
        for(let key in this.objMgr){
            if(this.objMgr[key].lateUpDate){
                this.objMgr[key].lateUpDate(dt);
            }
        }
    }
}

//导出一个函数，后续其他模块require到的就可以直接拿到这个导出的函数。
let instance = null;
module.exports.getInstance = function(){
    if(!instance){
        instance  = new Managers();
    }
    return instance;
}



//导出类名，那么后续其他模块里面require拿到的就是这个类。
//module.exports = Managers;


// let obj = {
//     id:1001,
//     name:'你大爷'
// }

// module.exports = obj;