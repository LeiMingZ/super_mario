class DataMgr{
    constructor(){
        this.objMgr={};
    }
    addManager(name,obj){
        if(!this.objMgr[name]){
            this.objMgr[name]=obj;
        }
    }
    getManager(name){
        return this.objMgr[name];
    }
    removeManager(name){
        delete this,this.objMgr[name];
    }
    clear(){
        for(let key in this.objMgr){
            delete this.objMgr[key];
        }
    }
}

let instance=null;
module.exports.getInstance=()=>{
    if(!instance){
        instance=new DataMgr();
    }
    return instance;
}