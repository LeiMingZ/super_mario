import { createDtMgr } from "./Common/Utils";
import getDataMgr from "./Manager/DataMgr";
const { ccclass, property } = cc._decorator;

@ccclass
export default class Load extends cc.Component {
  @property({
    displayName: "进度条",
    type: cc.ProgressBar,
  })
  public loadBar: cc.ProgressBar = null;

  public start(): void {
    createDtMgr("MapDtMgr", getDataMgr());
    createDtMgr("PrefabDtMgr", getDataMgr());
    createDtMgr("JsonDtMgr", getDataMgr());
    cc.resources.loadDir(
      "./",
      (finish, total, item) => {
        this.loadBar.progress = finish / total;
      },
      (error, assets) => {
        if (error) {
          return;
        }
        for (let value of assets) {
          if (value instanceof cc.TiledMapAsset) {
            getDataMgr().getManager("MapDtMgr").addData(value.name, value);
          } else if (value instanceof cc.Prefab) {
            getDataMgr().getManager("PrefabDtMgr").addData(value.name, value);
          } else if (value instanceof cc.JsonAsset) {
            getDataMgr()
              .getManager("JsonDtMgr")
              .addData(value.name, value.json);
          }
        }
        cc.find("Canvas/Root/Game").active = true;
      }
    );
  }
}
